import { NgModule } from '@angular/core';
import { DomePageModule } from './dome-page/dome-page.module';

@NgModule({
  imports: [
    DomePageModule
  ]
})
export class ProjectsModule { }
