import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DomePageComponent } from 'src/projects/dome-page/dome-page.component';
import { AdminAppRouterOutLetComponent } from './admin-app-router-outlet-component';


const routes: Routes = [
  {
    path: 'admin',
    component: AdminAppRouterOutLetComponent,
    children: [
      {
        path: 'dome-page',
        component: DomePageComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
