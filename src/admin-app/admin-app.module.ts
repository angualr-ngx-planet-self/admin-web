import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ProjectsModule } from 'src/projects/projects.module';
import { AdminAppRouterOutLetComponent } from './admin-app-router-outlet-component';

import { AppRoutingModule } from './admin-app-routing.module';
import { AdminAppComponent } from './admin-app.component';

@NgModule({
  declarations: [
    AdminAppComponent,
    AdminAppRouterOutLetComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ProjectsModule
  ],
  providers: [],
  bootstrap: [AdminAppComponent]
})
export class AdminAppModule { }
