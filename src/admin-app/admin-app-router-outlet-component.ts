import { Component } from '@angular/core';

@Component({
  selector: 'admin-app-router-outlet',
  template: '<router-outlet></router-outlet>'
})
export class AdminAppRouterOutLetComponent {
}
